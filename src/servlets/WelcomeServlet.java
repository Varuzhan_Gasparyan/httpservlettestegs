package servlets;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

public class WelcomeServlet extends HttpServlet {
   UserDAO userDAO = new UserDAO();

    public void doGet (HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException{
        response.setContentType("text/html");
        PrintWriter out =response.getWriter();

    }

    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException{
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        String firstname =  request.getParameter( "firstname");
        String lastname =  request.getParameter( "lastname");
        out.println("<HTML>");
        out.println("<TITLE> </TITLE>");
        out.println("<BODY>");
        out.println( " Welcome" + firstname + lastname );
        out.println("</BODY>");
        out.println("</HTML");
        if (firstname != null && lastname != null){
            try {
                userDAO.insertUser(new User(firstname,lastname));
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}
