package servlets;

public class User {
    private int id;
    private String lastName;
    private String frstName;

    public User(String lastName, String frstName) {
        this.lastName = lastName;
        this.frstName = frstName;
    }
    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFrstName() {
        return frstName;
    }

    public void setFrstName(String frstName) {
        this.frstName = frstName;
    }
}
