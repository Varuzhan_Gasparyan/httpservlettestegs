package servlets;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class UserDAO {
    static {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    private static Connection getConnection() throws SQLException {
        return DriverManager.getConnection("jdbc:mysql://localhost:3306/servletregistration?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC", "root", "root");
    }
    public void insertUser(User user) throws SQLException {

        try (Statement stmt = getConnection().createStatement()) {
            stmt.execute("INSERT INTO USER (lname,fname) VALUES ('"+ user.getLastName()+"','"+ user.getFrstName()
                    + "')");

        }
    }


}
